public class ECC_26_CheckArmStrong {
	
	public static void main(String[] arg) {
		int num = 1634;
		System.out.println(checkArmStrong(num));
	}

	public static String checkArmStrong(int num) {
		int temp,sum = 0, rem;
		
		if(num < 0)
			return "-1";
		
		
		if(num < 1000 || num > 9999)
			return "-2";
		else
		{
			temp = num;
			
			while( temp > 0) {
				
				rem = temp % 10;
				sum = sum + (rem * rem * rem * rem);
				temp = temp / 10;
				
				
			}
			
		}
		
		
		
		if(sum == num)
		
			return "ArmStrong Number";
		else
			return "Not ArmStrong Number";
	}
}