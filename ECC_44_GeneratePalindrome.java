
public class ECC_44_GeneratePalindrome {


	public static void main(String args[]) {
		
		
		int num = 211;
		System.out.println(getPalindromeList(num));
		
	}
	
	
	
	public static String getPalindromeList(int num) {
	
		int reverseNumber, sum = 0;
		int count = 0;
		
		if( num <= 0)
			return "Error";
		if(num < 100 || num > 999)
			return "Error";
		
		
		
		
		String result  = String.valueOf(num) + ",";
		
	
			while(!isPalindrome(num)) {
				
				sum = 0;
				reverseNumber = getReverse(num);
				sum = num + reverseNumber;
				result += reverseNumber + "," + sum + ",";
				num = sum;
				count ++;
				
				
				if(count > 10)
					return result.substring(0, result.length()-1);
			}
		
		
		
		
		
		return result.substring(0, result.length()-1);
	}
	
	
	 public static int getReverse(int n) {
	       int reverse = 0;
	    	
	       while(n > 0) {
	    	   reverse = (n % 10) + (reverse* 10) ;
	    	   n /= 10;
	    		
	    	}
	      
	       return reverse;
	    }
	 
	 
	 public static boolean isPalindrome(int num) {
		 
		 
		 return num == getReverse(num);
		 
	 }

}