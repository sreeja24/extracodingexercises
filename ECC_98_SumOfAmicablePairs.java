public class ECC_98_SumOfAmicablePairs {
    public static void main(String[] args) {
        System.out.println(sumOfAmicablePairs());
    }
    // return the sum of all amicable pairs till 1 Million.
    
    
    public static int sumOfAmicablePairs() {
    	
    	int sum = 0;
    	
        for( int num =1; num < 1000000; num++) {
        	
        	
        	int currentSum = getSumOfProperDivisors(num);
        	
        	if(num == getSumOfProperDivisors(currentSum)) {
        		System.out.println(num);
        		sum += num;
        		
        	}
        		 
        	
        }
    	
    	
    	return sum;
    }

    // return the sum of proper divisors for the given number
    public static int getSumOfProperDivisors(int num) {
        
    	int sum = 0;
    		for(int i = 1; i < num; i++)
    			if(num % i == 0)
    				sum += i;
    	
    	return sum;
    }
}