public class ECC_02_DigitChecker {
    public static void main(String[] args) {
        int num = 38;
        System.out.println(getDiffOfDigits(num));
        
        num = -2;
        System.out.println(getDiffOfDigits(num));
        
        num = 123;
        System.out.println(getDiffOfDigits(num));
        
        num = 3;
        System.out.println(getDiffOfDigits(num));
    } 
    
    
    public static int getDiffOfDigits(int num) {
    	
    	
    	int diff = 0, rem = 0, units, tens;
    	
    	if( num < 0)
    		return -3;
    	
    	if( num > 99)
    		return -2;
    	
    	if( num >= 0 && num <= 9 )
    		return -1;
    	
    	
    	else if(num>=10&&num<=99){
    		
    	units = num % 10;
    	tens = num/ 10;
    	diff=tens-units;
    	
    	}
    	
    	
    	
		return diff;
	// ADD YOUR CODE HERE
    }
}