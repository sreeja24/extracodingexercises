

public class ECC_47_OddSum {
	@SuppressWarnings("unused")
	public static int getOddSum(int[] inputArray)
	{
		int even = 0, nullcount = 0, oddsum = 0;	
		
		if(inputArray == null) 
			return -4;	
			
		if(inputArray.length != 5)
				return -1;
		else {	
			
			for(int value : inputArray) {
				if(value <= 0)
					return -2;
				
				if(value % 2 == 0)
					even++;
			}	
			
			
			
			if(even == inputArray.length)
				return -3;
			

			for(int i = 0; i < inputArray.length; i++) {
				
				if(inputArray[i] % 2 != 0)
					oddsum += inputArray[i];
				
		
			}
		
		}	
		
		
			
		
		
		
		return oddsum;
	}
	
	
	
	public static void main(String[] args) {
		
		int array[] = null;
		
		System.out.println(getOddSum(array));

	}
}