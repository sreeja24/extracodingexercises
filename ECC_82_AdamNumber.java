public class ECC_82_AdamNumber {
    public static void main(String[] args) {
        int num = 21;
        System.out.println(isAdamNumber(num));
    }
    
    // return true if the given number is an Adam Number
    public static boolean isAdamNumber(int num) {
        
    	int reverseNum, reverseSquare, square ;
    	
    	square = getSquare(num);
    	reverseNum  = getReverse(num);
    	reverseSquare = getSquare(reverseNum);
    	
    	return square == getReverse(reverseSquare);
    	
    }

    // return the reverse of the given number
    public static int getReverse(int n) {
       int reverse = 0;
    	
       while(n > 0) {
    	   reverse = (n % 10) + (reverse* 10) ;
    	   n /= 10;
    		
    	}
       
       return reverse;
    }

    // return the square of the give number
    public static int getSquare(int n) {
        return n*n;
    }
}