
public class ECC_57_ArrayRowSum {

	public static void main(String[] args) {
		int[] res = getRowSum(new int[][]{{10, 20, 30},{40,50, 60}, {70, 80, 90}});
		for(int i : res){
			System.out.print(i + " ");
		}
	}
	
	public static int[] getRowSum(int[][] arr){
		
		int rowsum[] = new int[3];
		int sum = 0;
		
		
		if( arr != null ) {

			if(arr.length == 3 && arr[0].length == 3 ) {
					
				for(int r = 0; r < arr.length; r++) {
					 sum = 0;
					for(int c = 0; c < arr.length; c++){
						sum += arr[r][c];
						
					}
					rowsum[r]=sum;
				}
				
				
				
			}
			else
				return null;
		}
		else 
			return null;
		
			
		
		return rowsum;
	}
}