import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ECC_53_Encryption {

	public static void main(String[] args) {
		//System.out.println(encrypt(null));
		System.out.println(encrypt("aa"));


	}

	public static String encrypt(String plainText) {
		
		if(plainText == null)
			return null;
		
		String str = "abcdefghijklmnopqrstuvwxyz";
		String rev = "zyxwvutsrqponmlkjihgfedcba";
		String output = "";
		int count=0;
		
		for (int j = 0; j < plainText.length(); j++) {
			
			if (plainText.charAt(j) >= 97 && plainText.charAt(j) <= 122) {

				count++;
			}
		}
		if (count==plainText.length()) {
			for (int j = 0; j < plainText.length(); j++) {

				for (int i = 0; i < str.length(); i++) {
					if (plainText.charAt(j) == str.charAt(i)) {
						output += rev.charAt(i);
					}
				}

			}
			return output;
		} 
		else
			return null;
		
		
	}

}