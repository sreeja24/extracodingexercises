
public class ECC_58_ArrayColumnSum {

	public static void main(String[] args) {
		int[] res = getColumnSum(new int[][]{{10, 20, 30},{40,50,60},{70,80,90}});
		for(int i : res){
			System.out.println(i);
		}
	}
	
	public static int[] getColumnSum(int[][] arr){
		int columnsum[] = new int[3];
		int sum = 0;
		
		
		if( arr != null ) {

			if(arr.length == 3 && arr[0].length == 3 ) {
				
				for(int r = 0; r < arr.length; r++) {
					 sum = 0;
					for(int c = 0; c < arr.length; c++){
						sum += arr[c][r];
						
					}
					columnsum[r]=sum;
				}
				
				
				
			}
			else
				return null;
		}
		else 
			return null;
		
			
		
		return columnsum;		
	}
}