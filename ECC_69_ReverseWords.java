
public class ECC_69_ReverseWords {

	public static void main(String[] args) {
		System.out.println(ECC_69_ReverseWords.reverse("talent sprint"));
	}

	public static String reverse(String str) {
			String result = " ";
			
		if(str == null  )
				return null;
		
		else {
			
			if(str.isEmpty())
				return null;
			else {
				
				String words[] = str.split(" ");
				
				for(String word : words ) {
					for(int i = word.length()-1; i>= 0 ; i--) {
						result += word.charAt(i);
					}
					result += " ";
				}
				return result.trim();
				
			}
				
		}		
			
	}

}