
public class ECC_68_SpinningString {

	public static void  main(String[] arg) {
		System.out.println(ECC_68_SpinningString.rotate("talent", 1));
		System.out.println(ECC_68_SpinningString.rotate("talent", 2));
		System.out.println(ECC_68_SpinningString.rotate("talent", 3));
		
		
	}

	public static String rotate(String str, int no_of_positions) {
		
		String result = "";
		if(str == null || str.isEmpty())
			return null;
		
		if(no_of_positions <= 0 )
			return str;
		
		if(no_of_positions > str.length())
			return str;
		else {
			
			char array[] = new char[str.length()];
			int j = no_of_positions;
			
			for(int i = 0; i < str.length(); i++) {
				
				if(j >= array.length)
					 j = 0;
				 array[j] = str.charAt(i);
				 j ++;
				 
				 
				
			}
					
			result  = String.valueOf(array);
				
		}
	
		return result.trim();
	}
}