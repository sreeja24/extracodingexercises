public class ECC_18_ReverseOrder {

	public static void main(String[] arg) {
		int start_val = 50;
		int end_val = 39;
		System.out.println(getNumbersInRange(start_val, end_val));
	}

    public static String getNumbersInRange(int s_val, int e_val) {

    	String result="";
    	
    	
    	if( s_val < 0 || e_val < 0)
    		return "-1";
    	
    	if( s_val == e_val)
    		return "-2";
    	
    	if(s_val < e_val)
    		return "-3";
    	
    	if(s_val-e_val == 1)
    		return "-4";
    	else {
    		
    		for(int i = s_val-1; i > e_val; i--) {
    			
    			result += " " + i;
    		}
    	}
    	
    	
    	return result.trim();
	}
}