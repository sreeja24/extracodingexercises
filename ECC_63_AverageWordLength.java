
public class ECC_63_AverageWordLength {

	public static void main(String[] args) {
		System.out.println(ECC_63_AverageWordLength.getAverageWordLength("Hi mom"));
		System.out.println(ECC_63_AverageWordLength.getAverageWordLength("hi everyone"));
		System.out.println(ECC_63_AverageWordLength.getAverageWordLength(null));
		System.out.println(ECC_63_AverageWordLength.getAverageWordLength(""));
		
	}

	public static int getAverageWordLength(String str) {
		
		if(str == null)
			return -1;
		
		if(str.isEmpty())
			return 0;
		else {
			
			int characters = 0;
			int words = 0;
			
			String s[] = str.split(" ");
			words = s.length;
			for(String value : s )
				characters += value.length();
			
			
			return characters / words;
			
		}
		
		
		
		
		
		
		// ADD YOUR CODE HERE
	}

}