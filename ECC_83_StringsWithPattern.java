import java.util.ArrayList;

public class ECC_83_StringsWithPattern {
    /* This program should print the list of string matching with pattern, if no string found matching pattern then print "No Strings Found with Pattern" */

    public static void main(String[] args) {
        String[] words = {"South Africa", "Afghanistan", "Sri Lanka", "New ZEALAND", "West Indies", "England", "India", "Australia", "Pakistan", "Bangladesh"};
        String pattern = "an";
        String[] words_pattern = getWordsContainsPattern(words, pattern);
        if (words_pattern.length > 0) {
            for (String word : words_pattern) {
                System.out.println(word);
            }    
        } else {
            System.out.println("No Strings Found with Pattern");
        }
    }
 
    // returns the array of string, containing strings with pattern matching
    public static String[] getWordsContainsPattern(String[] words, String pattern) {
        ArrayList<String> result = new ArrayList<String>(); 
        int i = 0;
        for(String word : words) {
        	
        	if(word.toUpperCase().contains(pattern.toUpperCase())) {
        		
        		result.add(word.toUpperCase() + " ");
        		i++;
        	}
        	
        }
        	String result1[] = new String[i]; 
        
        	return  result.toArray(result1);
    }
}