import java.util.Arrays;

public class ECC_49_FindMaximum {
	
	
		
	
			
			public static int findMax(int[] input) {
				
				
				
				int negcount = 0, max = 0;
				
				if(input != null) {
					
					for(int value : input) {
						
						if(value < 0)
							negcount ++;
					}
				}
				else
					return 0;
					

					if(negcount >= 3 ) {
						max = input[0];
						for(int  value : input) {
							if(value > max)
								max = value;
						}
						
						
					}
					else
						return -1;
									
				
					return max;
				
			}

		
	public static void main(String[] args) {
		
		int array[] = {1,2,6,-1,-2,-3};
		System.out.println(findMax(array));
		
	}
}