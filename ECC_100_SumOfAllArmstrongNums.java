


public class ECC_100_SumOfAllArmstrongNums {
    public static void main(String[] args) {
        int start_val = 100;
        int end_val = 10000;
        System.out.println(getSumOfAllArmstrongNums(start_val, end_val));
    }

    // return the sum of all armstrong numbers which contains all odd digits with in the given range
    public static int getSumOfAllArmstrongNums(int start_val, int end_val) {
        
    	
    	
    	
    	if(start_val < 0 || end_val < 0 )
    		return -1;
    	
    	else if(start_val == 0 || end_val == 0)
    		return -2;
    	
    	else if(start_val > end_val)
    		return -3;
    	
    	
    	
    	else {
    		int sum = 0;
    		for(int i = start_val; i < end_val; i++) {
    	
    		
    		if(isArmstrong(i)) {
    			
    			if(containsAllOddDigits(i)) {
    				
    				sum += i;
    				
    			}
    			
    		}
    	}
    		return sum;
    	}
    	
    	
    		
    }

    // return true if the given number is armstrong
    public static boolean isArmstrong(int num) {
    	
    	return num == sumOfPowersOfDigits(num);
        
    }

    // return the sum of powers of digits of the given number
    public static int sumOfPowersOfDigits(int num) {
         
    		int digits[] = getDigits(num);
    		int sum = 0;
    		
    		for(int i = 0; i < digits.length; i++) {
    			
    			sum = sum + (int)Math.pow(digits[i], digits.length);
    		}
    		
    		
    		return sum;
    }

    // return an array contains the digits of the number in the same order
    // if num = 153 return {1, 5, 3}
    public static int[] getDigits(int num) {
        
        int len = 0,temp;
        temp = num;
        
        
        while(temp > 0) {
        	len ++;
        	temp /= 10;
        }
        
        
        int digits[] = new int[len];
        
        
        	int i = len-1;
    	   
    	   while(num > 0) {
    		   len ++;
           	digits[i] = (num % 10);
           	i--;
           	num = num / 10;
           }
    	   
       
       
       return digits;
    	 
    }

    // return true if the number contains all odd digits.
    public static boolean containsAllOddDigits(int num) {
       
    	
    	int digits[] = getDigits(num);
		
		 
		
		
		for(int i = 0; i < digits.length; i++) {
			
			if(digits[i] % 2 == 0) {
				
				return false;
			}
				
		}
    	
    	return true;
    }
}