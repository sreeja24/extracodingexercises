public class ECC_12_SumOfMul10 {
	public static void main(String[] args) {
		int a = 23, b = 30, c = 69;
		System.out.println(sumOfMultiples(a, b, c));
	}

	public static int sumOfMultiples(int a, int b, int c) {
    	
    	if(a <= 0 || b <= 0 || c <= 0)
    		return -1;
    	
    	else {
    	
    		
    		if( a % 10 == 0)
    			a = a;
    		else 
    			a = (a / 10) * 10 + 10;
    		
    		
    		if( b % 10 == 0)
    			b = b;
    		else 
    			b = (b / 10) * 10 + 10;
    		
    		
    		if( c % 10 == 0)
    			c = c;
    		else 
    			c = (c / 10) * 10 + 10;
    		
    		
    		
    		
    	}
    	
    	return a + b + c;

    }
}